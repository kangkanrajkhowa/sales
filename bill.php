<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sales & Purchase</title>
    <link rel="stylesheet" type="text/css" href="./resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./resources/css/custom.css">
</head>

<body>
    <div class="container">
        <div class="invoice-body">
            <table class="w-100" style="border-bottom:2px solid red">
                <tr>
                    <td>
                        <img src="./resources/images/logo.png" class="invoice-logo">
                    </td>

                    <td class="invoice-address float-right">
                        <p><b>Sunny Deol</b></p>
                        <p>65, Parathewali Gali, Mathurapur<br>
                            Chandni Chowk, Delhi-6</p>
                        <p>GSTIN: 18-AADCT-7587B-1-Z-5</p>
                    </td>
                </tr>
            </table>
            <table class="w-100 mt-3">
                <tr>
                    <td class="invoice-receipent" width="35%">
                        <p><b>Name:</b> <b>Chris Evans</b></p>
                        <p><b>Address:</b> 10, Jacob West Street, Laos Town,
                            Newyork City-56</p>
                        <p><b>Customer GSTIN:</b> 18AABCA7577N171</p>
                        <p><b>Place of Supply:</b> Guwahati, Assam</p>
                        <p><b>Reverse Charge (Yes/No):</b> No</p>
                    </td>

                    <td class="invoice-receipent float-right" max-width="50%">
                        <p><b>Invoice No:</b> <b> TTS/BILL/AEDC/102</b></p>
                        <p><b>Invoice Date:</b> 01-Oct-2019</p>
                        <p><b>Order Ref No:</b> AMT/PO/IT/TRADING/1819/22</p>
                        <p><b>Order Date:</b> 01-Oct-2019</p>
                    </td>
                </tr>
            </table>
            <table class="w-100 table-bordered mt-2">
                <tr>
                    <th colspan="7">Product-wise Details:</th>
                </tr>

                <tr class="text-center" style="background-color: #eee">
                    <th width="4%">Sl. No.</th>
                    <th width="30%">Description</th>
                    <th width="13%">HSN/SAC Code</th>
                    <th width="8%">Qty</th>
                    <th width="8%">Unit</th>
                    <th>Rate</th>
                    <th>Total Sale</th>
                </tr>
                <tr>
                    <td width="4%" class="text-center">1</td>
                    <td width="40%" class="text-justify">
                        <p style="font-weight:bold; margin-bottom:0; margin-left:5px; margin-right:5px">Video
                            Cenferencing Polycom G310</p>
                        <p style="font-weight:normal;margin-left:5px; margin-right:5px">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    </td>
                    <td class="text-center">84715000</td>
                    <td class="text-center">2</td>
                    <td class="text-center">Nos.</td>
                    <td class="text-center">260.30</td>
                    <td class="text-center font-weight-bold">520.60</td>
                </tr>
                <tr>
                    <td width="4%" class="text-center">2</td>
                    <td width="40%" class="text-justify">
                        <p style="font-weight:bold; margin-bottom:0; margin-left:5px; margin-right:5px">Projector: BenQ
                            MX611</p>
                        <p style="font-weight:normal;margin-left:5px; margin-right:5px">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit, sed do eiusmod tempor.</p>
                    </td>
                    <td class="text-center">54787</td>
                    <td class="text-center">1</td>
                    <td class="text-center">Nos.</td>
                    <td class="text-center">72,584.00</td>
                    <td class="text-center font-weight-bold">72,584.00</td>
                </tr>
                <tr>
                    <td width="4%" class="text-center">3</td>
                    <td width="40%" class="text-justify">
                        <p style="font-weight:bold; margin-bottom:0; margin-left:5px; margin-right:5px">Logitech USB
                            Mouse</p>
                        <p style="font-weight:normal;margin-left:5px; margin-right:5px">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit, sed do eiusmod tempor.</p>
                    </td>
                    <td class="text-center">54787</td>
                    <td class="text-center">1</td>
                    <td class="text-center">Nos.</td>
                    <td class="text-center">72,584.00</td>
                    <td class="text-center font-weight-bold">72,584.00</td>
                </tr>
                <tr>
                    <td colspan="6" class="text-right font-weight-bold pr-3">Total</td>

                    <td class="text-center font-weight-bold">72,584.00</td>
                </tr>
            </table>
            <table class="w-100 table-bordered mt-2 text-center" style="font-size:14px;">
                <tr style="background-color: #eee;">
                    <th rowspan="2" width="7%">Sl. No.</th>
                    <th rowspan="2">Taxable Amount</th>
                    <th colspan="2">CGST</th>
                    <th colspan="2">SGST</th>
                    <th colspan="2">IGST</th>

                </tr>
                <tr style="background-color: #eee">
                    <th>Rate(%)</th>
                    <th>Amount</th>
                    <th>Rate(%)</td>
                    <th>Amount</td>
                    <th>Rate(%)</td>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>520.60</td>
                    <td>5%</td>
                    <td>500</td>
                    <td>14%</td>
                    <td>657</td>
                    <td>12%</td>
                    <td>432</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>72584.00</td>
                    <td>5%</td>
                    <td>500</td>
                    <td>14%</td>
                    <td>657</td>
                    <td>12%</td>
                    <td>432</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>72584.00</td>
                    <td>5%</td>
                    <td>500</td>
                    <td>14%</td>
                    <td>657</td>
                    <td>12%</td>
                    <td>432</td>
                </tr>
                <tr class="font-weight-bold">
                    <td>Total</td>
                    <td>72584.00</td>
                    <td></td>
                    <td>1500</td>
                    <td></td>
                    <td>1971</td>
                    <td></td>
                    <td>1296</td>
                </tr>
            </table>
            <table class="w-100 text-center mt-4">
                <tr>
                    <th width="70%" rowspan="7"></th>
                    <th style="background-color: #eee">Summary</th>
                    <th style="background-color: #eee">Value</th>
                </tr>
                <tr>
                    <td class="font-weight-bold">Total Taxable Value</td>
                    <td>78,524.00</td>
                </tr>
                <tr>
                    <td>Total CGST</td>
                    <td>5,600.00</td>
                </tr>
                <tr>
                    <td>Total SGST</td>
                    <td>5,600.00</td>
                </tr>
                <tr>
                    <td>Total IGST</td>
                    <td>5,600;00</td>
                </tr>
                <tr>
                    <td>Total GST</td>
                    <td>65,884.00</td>
                </tr>
                <tr style="border-top: 2px solid">
                    <td class="font-weight-bold">Invoice Value</td>
                    <td class="font-weight-bold">78,524.00</td>
                </tr>
            </table>
            <table class="w-100 mt-3">
                <td class="text-center font-weight-bold">
                    (Rupees..........................................................................................
                    only)</td>
            </table>
            <table class="w-100 table-bordered mt-4">
                <tr>
                    <td width="50%">
                        <p class="ml-2 mb-0" style="font-size:13px">1. This is test</p>
                        <p class="ml-2 mb-0" style="font-size:13px">2. This is test</p>
                        <p class="ml-2 mb-0" style="font-size:13px">3. This is test</p>
                        <p class="ml-2 mb-0" style="font-size:13px">4. This is test</p>
                        <p class="ml-2 mb-0" style="font-size:13px">5. This is test</p>
                    </td>
                    <td class="text-center">
                    <p>For Trans Technologies Solution Pvt. Ltd.</p>
                        <p class="mt-5">Authorized Signatory</p>
                    </td>
                </tr>
            </table>
            <footer>
                <p class="mt-3 text-center">Subject to Guwahati Jurisdiction.</p>
                <p class="m-0 text-center">This is a Computer Generated Invoice.</p>
            </footer>
        </div>
    </div>





    <script src="./resources/js/jquery.js"></script>
    <script src="./resources/js/popper.js"></script>
    <script src="./resources/js/bootstrap.js"></script>
</body>

</html>