
<?php
include './header.php';
?>

<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Client</li>
        </ol>
    </nav>


    <div>
        <a href="./new-client.php"><button class="btn btn-lg btn-outline-primary mb-3">Create New
                Client</button></a>
    </div>
    <table class="table table-bordered table-hover text-center table-sm table-responsive">
        <thead>
            <tr style="background: #eee">
                <th>#</th>
                <th width="10%">Organization Name</th>
                <th>Contact Person</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Company Website</th>
                <th>GST No.</th>
                <th>Address</th>
                <th>Pincode</th>
                <th>State</th>
                <th width="7%">Action</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Amtron Ltd.</td>
                <td>Margerita Jason</td>
                <td>someone@example.com</td>
                <td>7002525478</td>
                <td>www.example.co.in</td>
                <td>GST927374899303</td>
                <td>Zoo Tiniali, Guwahati</td>
                <td>781021</td>
                <td>Maharashtra</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Amtron Ltd.</td>
                <td>Margerita Jason</td>
                <td>someone@example.com</td>
                <td>7002525478</td>
                <td>www.example.co.in</td>
                <td>GST927374899303</td>
                <td>Zoo Tiniali, Guwahati</td>
                <td>781021</td>
                <td>Maharashtra</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Amtron Ltd.</td>
                <td>Margerita Jason</td>
                <td>someone@example.com</td>
                <td>7002525478</td>
                <td>www.example.co.in</td>
                <td>GST927374899303</td>
                <td>Zoo Tiniali, Guwahati</td>
                <td>781021</td>
                <td>Maharashtra</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
        </tbody>
    </table>

</div>


<?php
include './footer.php';
?>

