<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sales & Purchase</title>
    <link rel="stylesheet" type="text/css" href="./resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./resources/css/custom.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<body>

    <nav class="navbar navbar-expand-lg bg-primary">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <img src="./resources/images/menu.png" alt="menu">
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="./dashboard.php">
                        <i class="fas fa-home"></i> Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="./quotation.php"><i class="fas fa-book"></i> Quotation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="./purchase.php"><i class="fa fa-box"></i> Purchase</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="./sales.php"><i class="fa fa-box-open"></i> Sales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="./report.php"><i class="fas fa-print"></i> Report</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="./gst.php"><i class="fas fa-calculator"></i> GST</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link font-weight-bold" href="./logout.php"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </li>
            </ul>

        </div>
    </nav>