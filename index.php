<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login : Sales & Purchase</title>
    <link rel="stylesheet" type="text/css" href="./resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./resources/css/custom.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<body style="background:whitesmoke">
    <div class="container login-card">
        <div class="row">
            <div class="offset-md-4 col-md-4 col-lg-4 offset-lg-4">
                <div class="form-login">
                    <h4 class="login-head">LOGIN</h4>
                    <input type="text" name="username" class="form-control input-sm my-3" placeholder="Enter username" />
                    <input type="text" name="password" class="form-control input-sm my-3 mb-4"
                        placeholder="Enter password" />
                    
                    <div class="btn-login">
                            <a href="#" class="btn btn-primary btn-lg">Go to dashboard <i class="fa fa-sign-in-alt"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <script src="./resources/js/jquery.js"></script>
    <script src="./resources/js/popper.js"></script>
    <script src="./resources/js/bootstrap.js"></script>
</body>

</html>