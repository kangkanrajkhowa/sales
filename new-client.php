<?php
    include './header.php';
?>
<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
            <li class="breadcrumb-item"><a href="./quotation.php">Client</a></li>
            <li class="breadcrumb-item active" aria-current="page">New Client</li>
        </ol>
    </nav>
    <h1 class="text-center pb-3 text-primary grad-text">Create a new client</h1>
    <form action="" method="" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-4 offset-lg-2 offset-md-2 col-md-4 col-sm-12 my-2">
                <div class="card">
                    <div class="card-header">
                        <h3>Client Info</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Organization Name</label>
                            <input type="text" class="form-control form-control-sm"
                                placeholder="Enter organization name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Website</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Enter reference no.">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">GST No.</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Enter type">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 my-2">
                <div class="card">
                    <div class="card-header">
                        <h3>Contact Info</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Contact Person Name</label>
                            <input type="text" class="form-control form-control-sm"
                                placeholder="Enter contact person name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Email</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Enter contact email">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Contact No</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Enter contact no.">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-2 offset-md-2 col-md-4 col-sm-12 my-2">
                <div class="card">
                    <div class="card-header">
                        <h3>Address Info</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Address(Street, Vill, Town, District)</label>
                            <textarea class="form-control form-control-sm"
                                placeholder="Enter address"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">Pincode</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Enter pincode">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label font-weight-bold">State</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Enter state">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 offset-lg-9 col-md-1 offset-md-8 col-sm-12 text-center">
            <button class="btn btn-lg btn-outline-primary mt-1 mb-4">Create</button>
        </div>

    </form>

</div>




<?php
    include './footer.php';
?>