<?php
    include './header.php';
?>
<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
            <li class="breadcrumb-item"><a href="./quotation.php">Quotation</a></li>
            <li class="breadcrumb-item active" aria-current="page">New Quotation</li>
        </ol>
    </nav>
    <h1 class="text-center pb-3 text-primary grad-text">Create a new quotation</h1>
    <form action="" method="" enctype="multipart/form-data" class="col-lg-8 offest-lg-2 col-md-8 offset-md-2 col-sm-12">
        <div class="row">
            <div class="form-group col-md-6">
                <label class="col-form-label font-weight-bold">Quotation Date</label>
                <input type="date" class="form-control form-control-sm">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">TTS Ref. No.</label>
                <input type="text" class="form-control form-control-sm" placeholder="Enter reference no.">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">Type</label>
                <input type="text" class="form-control form-control-sm" placeholder="Enter type">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">NIQ No</label>
                <input type="text" class="form-control form-control-sm" placeholder="Enter NIQ no.">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">NIQ Date</label>
                <input type="date" class="form-control form-control-sm">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">Organization</label>
                <input type="text" class="form-control form-control-sm" placeholder="Enter name of organization">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">Narration</label>
                <textarea class="form-control form-control-sm" placeholder="Enter narration"></textarea>
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">Total Quotation Amt (Including Tax)</label>
                <input type="text" class="form-control form-control-sm" placeholder="Enter amount">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">Order Ref. No.</label>
                <input type="text" class="form-control form-control-sm" placeholder="Enter order reference no.">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <label class="col-form-label">Order Date</label>
                <input type="date" class="form-control form-control-sm">
            </div>
            <div class="form-group col-md-6 font-weight-bold">
                <p>Upload file</p>
                <input type="file" id="myFile" name="">
            </div>

        </div>
        <a href=""><button class="btn btn-lg btn-outline-primary float-right mt-1 mb-4">Create</button></a>
    </form>

</div>




<?php
    include './footer.php';
?>