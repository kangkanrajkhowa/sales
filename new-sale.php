<?php
    include './header.php';
?>
<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
            <li class="breadcrumb-item"><a href="./sales.php">Sales</a></li>
            <li class="breadcrumb-item active" aria-current="page">New Sale</li>
        </ol>
    </nav>
    <h1 class="text-center pb-3 text-primary grad-text">Create a new sale</h1>
</div>
<div class="container-fluid">
    <table id="myTable" class=" table order-list">
    <thead>
        <tr>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td>Name</td>
            <td></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-sm-2">
                <input type="text" name="" class="form-control" />
            </td>
            <td class="col-sm-2">
                <input type="mail" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1">
                <input type="text" name=""  class="form-control"/>
            </td>
            <td class="col-sm-1"><a class="deleteRow"></a>

            </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
               <button class="btn btn-lg btn-outline-success float-right" id="addrow" value="Add Row" >Add Row</button>
            </td>
            <td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
        </tr>
    </tfoot>
</table>
</div>




<?php
    include './footer.php';
?>