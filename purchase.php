<?php
include './header.php';
?>

<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Purchase</li>
        </ol>
    </nav>


    <div>
        <a href="./new-purchase.php"><button class="btn btn-lg btn-outline-primary mb-3">Create New
                Purchase</button></a>
    </div>
    <table class="table table-bordered table-hover text-center table-sm table-responsive">
        <thead>
            <tr style="background: #eee">
                <th rowspan="2">#</th>
                <th rowspan="2">Invoice Date</th>
                <th rowspan="2">Invoice No.</th>
                <th rowspan="2">Company</th>
                <th rowspan="2">GST No</th>
                <th rowspan="2">Description of Items</th>
                <th rowspan="2">Quantity</th>
                <th rowspan="2">Rate</th>
                <th rowspan="2">Sub-total</th>
                <th rowspan="2">Fees</th>
                <th colspan="2">CGST</th>
                <th colspan="2">SGST</th>
                <th colspan="2">IGST</th>
                <th rowspan="2">Total Amount</th>
                <th rowspan="2">Purpose</th>
                <th rowspan="2" width="7%">Action</th>
            </tr>
            <tr style="background: #eee">
                <th>Rate</th>
                <th>Amount</th>
                <th>Rate</td>
                <th>Amount</td>
                <th>Rate</td>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>01/12/2019</td>
                <td>TTS/SDC/2017/251</td>
                <td>Amtron Ltd.</td>
                <td>GST189438374IN</td>
                <td>Two piece USB mouse</td>
                <td>2</td>
                <td>5,00.00</td>
                <td>1,000.00</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>50</td>
                <td>1,000.00</td>
                <td>Official</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>01/12/2019</td>
                <td>TTS/SDC/2017/251</td>
                <td>Amtron Ltd.</td>
                <td>GST189438374IN</td>
                <td>Two piece USB mouse</td>
                <td>2</td>
                <td>5,00.00</td>
                <td>1,000.00</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>50</td>
                <td>1,000.00</td>
                <td>Official</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>01/12/2019</td>
                <td>TTS/SDC/2017/251</td>
                <td>Amtron Ltd.</td>
                <td>GST189438374IN</td>
                <td>Two piece USB mouse</td>
                <td>2</td>
                <td>5,00.00</td>
                <td>1,000.00</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>50</td>
                <td>1,000.00</td>
                <td>Official</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
        </tbody>
    </table>

</div>


<?php
include './footer.php';
?>