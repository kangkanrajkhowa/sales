<?php
include './header.php';
?>

<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Quotation</li>
        </ol>
    </nav>


    <div>
        <a href="./new-quotation.php"><button class="btn btn-lg btn-outline-primary mb-3">Create New
                Quotation</button></a>
    </div>
    <table class="table table-bordered table-hover text-center table-sm table-responsive">
        <thead>
            <tr style="background: #eee">
                <th scope="col">#</th>
                <th scope="col">Quotation Date</th>
                <th scope="col">TTS Ref. No.</th>
                <th scope="col">Type</th>
                <th scope="col">NIQ No</th>
                <th scope="col">NIQ Date</th>
                <th scope="col" width="8%">Organization</th>
                <th scope="col">Narration</th>
                <th scope="col">Total Quotation Amt (Including Tax)</th>
                <th scope="col">Order Ref. No.</th>
                <th scope="col">Order Date</th>
                <th scope="col" width="7%">Action</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>01/12/2019</td>
                <td>TTS/SDC/2017/251</td>
                <td>Renewal</td>
                <td>AS4584</td>
                <td>15/12/2017</td>
                <td>State Horticulture</td>
                <td>This is some testing line</td>
                <td>51,000.00</td>
                <td>IT/254/AZ/54</td>
                <td>10/12/2017</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>01/12/2019</td>
                <td>TTS/SDC/2017/251</td>
                <td>Renewal</td>
                <td>AS4584</td>
                <td>15/12/2017</td>
                <td>State Horticulture</td>
                <td>This is some testing line</td>
                <td>51,000.00</td>
                <td>IT/254/AZ/54</td>
                <td>10/12/2017</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>01/12/2019</td>
                <td>TTS/SDC/2017/251</td>
                <td>Renewal</td>
                <td>AS4584</td>
                <td>15/12/2017</td>
                <td>State Horticulture</td>
                <td>This is some testing line</td>
                <td>51,000.00</td>
                <td>IT/254/AZ/54</td>
                <td>10/12/2017</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
        </tbody>
    </table>

</div>


<?php
include './footer.php';
?>