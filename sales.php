<?php
include './header.php';
?>
<div class="container-fluid my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sales</li>
        </ol>
    </nav>


    <div>
        <a href="./new-sale.php"><button class="btn btn-lg btn-outline-primary mb-3">Create New
                Sale</button></a>
    </div>
    <table class="table table-bordered table-hover text-center table-sm table-responsive">
        <thead>
            <tr style="background: #eee">
                <th rowspan="2">#</th>
                <th rowspan="2">Invoice Date</th>
                <th rowspan="2">Invoice Ref. No.</th>
                <th rowspan="2">Organization</th>
                <th rowspan="2">Customer GSTIN</th>
                <th rowspan="2">Narration</th>
                <th rowspan="2">HSN Code</th>
                <th rowspan="2">Total Taxable Value</th>
                <th colspan="2">CGST</th>
                <th colspan="2">SGST/UTGST</th>
                <th colspan="2">IGST</th>
                <th rowspan="2">Total Amount</th>
                <th rowspan="2" width="6%">Action</th>

            </tr>
            <tr style="background: #eee">
                <th>Rate</th>
                <th>Amount</th>
                <th>Rate</td>
                <th>Amount</td>
                <th>Rate</td>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>01/09/2019</td>
                <td>TTS/ADGF/2019/102</td>
                <td>Directorate of Elementary Education</td>
                <td>GSTIN1819348Z98</td>
                <td>This is test and this is also test.</td>
                <td>8565447</td>
                <td>25,000.00</td>
                <td>9%</td>
                <td>1,550.00</td>
                <td>9%</td>
                <td>1,550.00</td>
                <td>0%</td>
                <td>0</td>
                <td>28,100.00</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>01/09/2019</td>
                <td>TTS/ADGF/2019/102</td>
                <td>Directorate of Elementary Education</td>
                <td>GSTIN1819348Z98</td>
                <td>This is test and this is also test.</td>
                <td>8565447</td>
                <td>25,000.00</td>
                <td>9%</td>
                <td>1,550.00</td>
                <td>9%</td>
                <td>1,550.00</td>
                <td>0%</td>
                <td>0</td>
                <td>28,100.00</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>01/09/2019</td>
                <td>TTS/ADGF/2019/102</td>
                <td>Directorate of Elementary Education</td>
                <td>GSTIN1819348Z98</td>
                <td>This is test and this is also test.</td>
                <td>8565447</td>
                <td>25,000.00</td>
                <td>9%</td>
                <td>1,550.00</td>
                <td>9%</td>
                <td>1,550.00</td>
                <td>0%</td>
                <td>0</td>
                <td>28,100.00</td>
                <td> <a href="#"><i class="fa fa-eye"></i></a>
                    <a href="#"><i class="fa fa-edit"></i></a>
                    <a href="#"><i class="fa fa-trash"></i></a></td>
            </tr>
           
        </tbody>
    </table>

</div>



<?php
include './footer.php';
?>
    